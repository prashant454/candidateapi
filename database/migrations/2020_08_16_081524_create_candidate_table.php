<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name',40)->notNullable();
            $table->string('last_name',40)->nullable();
            $table->string('email',100)->nullable();
            $table->string('contact_number',100)->nullable();
            $table->smallInteger('gender')->nullable();
            $table->string('specialization',200)->nullable();
            $table->smallInteger('work_ex_year')->nullable();
            $table->integer('candidate_dob')->nullable();
            $table->string('address',500)->nullable();
            $table->string('resume')->nullable();
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
