<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Candidate;

class CandidateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public static function addCadidate(Request $request){
        if($request['first_name']){
            $result=Candidate::addCadidate($request);
            if($result['success']==1)
                return response()->json($result, 200);
            else return response()->json($result, 400);
        }
        else return response()->json(["msg"=>"First name is required and missing in request"],400);
    }

    public static function getAll(Request $request){
        if($request['limit'] && $request['page'])
            return response()->json(Candidate::getAll($request), 200);
        else return response()->json(["msg"=>"Limit or page field is missing in request"],400);
    }

    public static function searchCandidates(Request $request){
        if($request['limit'] && $request['page'] && ($request['first_name'] || $request['last_name'] || $request['email']))
            return response()->json(Candidate::getAll($request), 200);
        else return response()->json(["msg"=>"Limit,page or one of search fields like first_name, last_name, email are missing in request"],400);
    }

    public static function getById($id){
        return response()->json(Candidate::getById($id), 200);
    }

    public static function post(Request $request){
        return response()->json(Candidate::select()->paginate($request['limit']), 200);
    }
}
