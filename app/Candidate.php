<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use \Carbon\Carbon;

class Candidate extends Model {

    const resume_path="/public/CandidateFiles/Resumes";
    const resume_url="/CandidateFiles/Resumes/";

    public static function addCadidate($request){
        $candidate = new Candidate();

        $candidate['first_name']=$request['first_name'];
        $candidate['last_name']=$request['last_name'];
        $candidate['email']=$request['email'];
        $candidate['contact_number']=$request['contact_number'];
        $candidate['gender']=$request['gender'];
        $candidate['specialization']=$request['specialization'];
        $candidate['work_ex_year']=$request['work_ex_year'];
        $candidate['candidate_dob']=$request['candidate_dob'];
        $candidate['address']=$request['address'];

        if($request['resume']!=null){
            // $fileHandle = fopen($request['resume'], "r");
            // $fileContent = fread($fileHandle, filesize($request['resume']));
            // $fileContent = addslashes($fileContent);
            // $candidate['resume']=$fileContent;

            $file=$request['resume'];
			$file_name = Carbon::now()->timestamp.rand(2,10)."_".$file->getClientOriginalName();
			$destinationPath = base_path(self::resume_path);
			$file->move($destinationPath,$file_name);
			$candidate['resume'] = self::resume_url.$file_name;
        }
        return $candidate->save()?["success"=>1,"msg"=>"Candidate created successfully"]:["success"=>0,"msg"=>"Candidate creation failed"];
    }
    
    public static function getAll($request){
       $query=self::select('first_name','last_name','email','contact_number','gender'
                            ,'specialization','work_ex_year','candidate_dob','address',
                            DB::raw("IF(ISNULL(resume),'', CONCAT('".url()."',resume)) as resume"));

       if($request['first_name'])
            $query=$query->where(DB::raw("Lower(first_name)"),"Like","%".strtolower($request['first_name'])."%");
       
       if($request['last_name'])
            $query=$query->OrWhere(DB::raw("Lower(last_name)"),"Like","%".strtolower($request['last_name'])."%");

       if($request['email'])
            $query=$query->OrWhere(DB::raw("Lower(email)"),"Like","%".strtolower($request['email'])."%");

       return $query->paginate($request['limit']);
    }

    public static function getById($id){
       return self::select('first_name','last_name','email','contact_number','gender'
                            ,'specialization','work_ex_year','candidate_dob','address',
                            DB::raw("IF(ISNULL(resume),'', CONCAT('".url()."',resume)) as resume"))
                            ->where('id',$id)
                            ->first();
    }
}